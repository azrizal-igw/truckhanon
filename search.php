<?php


if (!empty($_GET['search-key'])) {
	$skey = $_GET['search-key'];
} else {
	header("Location: index.html");
	die();
}


$response = file_get_contents('http://sensor.saifon.my/PrxAndroid/PrxAndroid.svc/GetMobiles/Netroo');

$response = json_decode($response);

$props = array();

foreach ( $response as $data )
{	
	if ($skey) {
		if( strpos( $data[2]->Value, strtoupper(trim($skey)) ) !== false ) {
			$arr['coords']['lat'] = $data[3]->Value;
			$arr['coords']['lng'] = $data[4]->Value;
			$arr['content'] = $data[2]->Value;
			
			array_push($props, $arr);
		}
	}
		
}

if (empty($props)) {
	header("Location: truck-not-found.html");
	die();
}
	

//echo json_encode($props);

?>

<!DOCTYPE html>
<html>
  <head>
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <style>
	
	body, html{
		font-family: 'Montserrat', sans-serif;
		height: 100%;
		margin: 0;
	}
	h1{
		text-transform: uppercase; letter-spacing: 0.06em; margin-bottom: 40px;
	}
	
	#map {
		width: 100%;
		height: 100%;
	}

    </style>
	<title>Food Truck Map Demo</title>
  </head>
  <body>
	<!-- navbar -->
	<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
		<a class="navbar-brand" href="index.html">Food Truck Map</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarsExampleDefault">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					
				</li>
			</ul>
		<form id="searchForm" class="form-inline my-2 my-lg-0" action="search.php">
			<input class="form-control mr-sm-2" type="text" name="search-key" placeholder="Search" aria-label="Search">
			<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
		</form>
		</div>
    </nav>
	
	<div id="map"></div>
	
	
	
	
	<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script>
	function initMap() {
		var cyberjaya = {lat: 2.9213, lng: 101.6559};
		
		/* base map */
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 12,
			center: cyberjaya
		});
		
		var bounds = new google.maps.LatLngBounds();
		
		var props = '<?php echo json_encode($props); ?>';
		
		var markers = JSON.parse(props).map(function(props, i) {
			bounds.extend(props.coords);
			
			return new google.maps.Marker({
				position: props.coords,
				map: map,
				icon: 'food-truck-icon.png',
				//content: props.content
				title: props.content
				//label: props.content
			});
		});
		
		$.each( markers, function( index, value ){
			console.log(value)
			
			var infoWindow = new google.maps.InfoWindow({
				content: value.title
			});
			infoWindow.open(map, value);
			
			value.addListener('mouseover', function() {
				infoWindow.open(map, value);
			});
		});
		
		// Add a marker clusterer to manage the markers.
		var markerCluster = new MarkerClusterer(map, markers, {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
		
		map.fitBounds(bounds);
					
		function addMarker(props) {
			var marker = new google.maps.Marker({
				position: props.coords,
				map: map,
				icon: 'food-truck-icon-2.png',
				title: props.content
			});
			
		}

	}
	
    </script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
	<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBr-poO582NlvethXHJ6ZaDYFXjZ5i1suE&callback=initMap">
	</script>
	
  </body>
</html>